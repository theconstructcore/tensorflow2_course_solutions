#!/usr/bin/env python

# We load Python stuff first because afterwards it will be removed to avoid error with openCV
import sys
sys.path.append('/usr/lib/python2.7/dist-packages')
import rospy
import rospkg


rospy.logdebug("Start Module Loading...Remove CV ROS-Kinetic version due to incompatibilities")
import csv

import glob
import os
import xml.etree.ElementTree as ET
import shutil

# We do this because of a bug in kinetic when importing cv2
import sys
#import subprocess
#new_proc = subprocess.Popen(["rosversion", "-d"], stdout=subprocess.PIPE)
#version_str = new_proc.communicate()[0]
#ros_version = version_str.decode('utf8').split("\n")[0]
ros_version = "kinetic"
if ros_version == "kinetic":
    try:
        sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
    except Exception as ex:
        print(ex)
        print("Its already removed..../opt/ros/kinetic/lib/python2.7/dist-packages")
import cv2

SPLIT_RATIO = 0.8

AUGMENTATION = False
AUGMENTATION_DEBUG = False
AUGMENTATION_PER_IMAGE = 25

try:
    import imgaug as ia
    from imgaug import augmenters as iaa
except ImportError:
    rospy.logdebug("Augmentation disabled")
    AUGMENTATION = False

rospy.logdebug("Loaded Modules...")



def generate_database_now(path_to_source_training_package, path_to_database_training_package):

    rospy.loginfo("Start main...")

    ######### START OF Directories Setup

    dataset_images_folder = os.path.join(path_to_source_training_package, "dataset_gen/images")
    dataset_annotations_folder = os.path.join(path_to_source_training_package, "dataset_gen_annotations")

    train_images_folder = os.path.join(path_to_database_training_package, "train")
    validation_images_folder = os.path.join(path_to_database_training_package, "validation")


    if not os.path.exists(dataset_images_folder):
        rospy.logfatal("Dataset not found==>"+str(dataset_images_folder)+", please run rosrun randomgazebomanager_pkg create_training_material.py")
        return False
    else:
        rospy.loginfo("Trainin Images path found ==>"+str(dataset_images_folder))

    # We clean up the training folders
    if os.path.exists(train_images_folder):
        shutil.rmtree(train_images_folder)
    os.makedirs(train_images_folder)
    rospy.loginfo("Created folder=" + str(train_images_folder))

    if os.path.exists(validation_images_folder):
        shutil.rmtree(validation_images_folder)
    os.makedirs(validation_images_folder)
    rospy.loginfo("Created folder=" + str(validation_images_folder))

    ######### END OF Directories Setup
    rospy.loginfo("END OF Directories Setup")


    ## START CSV generation
    s = 0
    for c in lengths:
        # We decide if it goes to train folder or to validate folder

        if i <= c * SPLIT_RATIO:
            basename = os.path.basename(data_list[0])
            train_scaled_img_path = os.path.join(train_images_folder, basename)
            data_list[0] = os.path.abspath(train_scaled_img_path)
            csv_train_writer.writerow(data_list)
        else:
            basename = os.path.basename(data_list[0])
            validate_scaled_img_path = os.path.join(validation_images_folder, basename)
            data_list[0] = os.path.abspath(validate_scaled_img_path)
            csv_validate_writer.writerow(data_list)

        image = cv2.imread(absolute_original_path)

        cv2.imwrite(data_list[0], cv2.resize(image, (image_size, image_size)))

        s += 1

    ## END CSV generation

    rospy.loginfo("\nDone!")

    return True


def main():

    rospy.init_node('generate_database_node', anonymous=True, log_level=rospy.INFO)
    rospy.logwarn("Generate Database...START")

    if len(sys.argv) < 4:
        rospy.logfatal("usage: generate_dataset.py path_to_source_training_package image_size path_to_database_training_package")
    else:
        path_to_source_training_package = sys.argv[1]
        image_size = int(sys.argv[2])
        path_to_database_training_package = sys.argv[3]

        if path_to_database_training_package == "None":
            rospack = rospkg.RosPack()
            # get the file path for dcnn_training_pkg
            path_to_database_training_package = rospack.get_path('dcnn_training_pkg')
            rospy.logwarn("NOT Found path_to_database_training_package, getting default:"+str(path_to_database_training_package))
        else:
            rospy.logwarn("Found path_to_database_training_package:"+str(path_to_database_training_package))

        rospy.logwarn("Path to Training Original Material:"+str(path_to_source_training_package))
        rospy.logwarn("image_size to generate database:"+str(image_size))
        rospy.logwarn("image_size to generate database:"+str(path_to_database_training_package))

        generate_database_now(  path_to_source_training_package,
                                image_size,
                                path_to_database_training_package)

        rospy.logwarn("Generate Database...END")


if __name__ == "__main__":
    main()
