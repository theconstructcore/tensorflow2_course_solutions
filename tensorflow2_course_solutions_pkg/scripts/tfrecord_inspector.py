#import tensorflow as tf
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

print("TRAIN.RECORD data ==>")
path_train = "data/train.tfrecord"
#for example in tf.python_io.tf_record_iterator("data/train.tfrecord"):
for example in tf.data.TFRecordDataset(path_train):
    #print(example)
    result = tf.train.Example.FromString(example)
    print(result)

print("TEST.RECORD data ==>")
#for example in tf.python_io.tf_record_iterator("data/test.tfrecord"):
path_test = "data/test.tfrecord"
for example in tf.data.TFRecordDataset(path_test):
    result = tf.train.Example.FromString(example)
    print(result)